import React from "react";
import { Block as BlockType } from "../types/Block";
import { styled } from "@mui/material/styles";
import { Box, Typography } from "@mui/material";

const Container = styled(Box)({
  backgroundColor: "rgba(0, 0, 0, 0.12)",
  marginBottom: "4px",
  padding: "8px",
  borderRadius: "2px",
});

const Index = styled(Typography)({
  fontStyle: "normal",
  fontWeight: "bold",
  fontSize: "10px",
  lineHeight: "16px",
  letterSpacing: "1.5px",
  color: "#304FFE",
});

const DataText = styled(Typography)({
  fontStyle: "normal",
  fontWeight: "normal",
  fontSize: "14px",
  lineHeight: "20px",
  letterSpacing: "0.25px",
  color: "#263238",
});

interface BlockProps {
  block: BlockType;
}

const Block: React.FC<BlockProps> = ({ block: { data, index } }) => {
  return (
    <Container>
      <Index> {`${index}`.padStart(3, "0")} </Index>
      <DataText>{data} </DataText>
    </Container>
  );
};

export default Block;
