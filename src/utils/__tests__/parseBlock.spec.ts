import parseBlock from "../parseBlock";
import { FullInfoBlock } from "../../types/Block";

describe("parseBlock", () => {
  it("Should parse data properly", () => {
    const testData: FullInfoBlock = {
      id: "aa",
      type: "eee",
      attributes: {
        index: 1,
        timestamp: 3888888,
        data: "Test",
        "previous-hash": "aaaaaaaaaa",
        hash: "aaaaaaaaa",
      }
    };

    expect(parseBlock(testData)).toEqual({
      index: 1,
      data: "Test",
    })
  });
});
