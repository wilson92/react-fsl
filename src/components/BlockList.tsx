import React from "react";
import { Block as BlockType } from "../types/Block";
import { Typography } from "@mui/material";
import { Node } from "../types/Node";
import Block from "./Block";

interface BlockListProps {
  node: Node;
}

const BlockList: React.FC<BlockListProps> = ({ node: { blocks, loading } }) => {
  if (loading) {
    return <Typography>Loading....</Typography>;
  }

  return (
    <>
      {blocks.length === 0 ? (
        <Typography>There is no blocks</Typography>
      ) : (
        <>
          {blocks.map((block: BlockType) => (
            <Block key={block.index} block={block} />
          ))}
        </>
      )}
    </>
  );
};

export default BlockList;
