import { Block, FullInfoBlock } from "../types/Block";

const parseBlock = (block: FullInfoBlock): Block => {
  const {
    attributes: { index, data },
  } = block;

  return { index, data };
};

export default parseBlock;
