export interface Block {
  index: number;
  data: string;
}

export type FullInfoBlock = {
  id: string;
  type: string;
  attributes: Block & {
    timestamp: number;
    "previous-hash": string;
    hash: string;
  };
};
